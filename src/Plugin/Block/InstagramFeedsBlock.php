<?php

namespace Drupal\social_feeds_block\Plugin\Block;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use GuzzleHttp\ClientInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Instagram feeds' block.
 *
 * @Block(
 *   id = "instagram_feeds_block",
 *   admin_label = @Translation("Instagram social feeds")
 * )
 */
class InstagramFeedsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Contains the configuration object factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configfactory;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, ClientInterface $http_client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configfactory = $config_factory;
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition, $container->get('config.factory'), $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $json_response = '';
    $error_message = '';

    $values = $this->configfactory->getEditable('config.instagram_social_feeds_block');

    // $insta_client_id = $values->get('insta_client_id');
    // $insta_redirec_uri = $values->get('insta_redirec_uri');.
    $insta_access_token = $values->get('insta_access_token');
    $insta_pic_counts = $values->get('insta_pic_counts');

    // $i = 0;
    // $images = $pic = array();
    $url = "https://api.instagram.com/v1/users/self/media/recent/?access_token=" . $insta_access_token . '&count=' . $insta_pic_counts;

    $response = $this->httpClient->get($url, ['headers' => ['Accept' => 'text/json']]);
    $request = $response->getBody()->getContents();

    $instagram_social_feeds = $this->configfactory->getEditable('config.instagram_social_feeds_block');
    $insta_image_resolution = $instagram_social_feeds->get('insta_image_resolution');
    $insta_likes = $instagram_social_feeds->get('insta_likes');

    if (isset($insta_access_token) && !empty($insta_access_token)) {
      if ($request->status_message != 'BAD REQUEST' || $request->status_message != 'BAD REQUEST') {
        $json_response = json_decode($request, TRUE);
      }
      else {
        $error_message = 'The access token provided is invalid.';
      }
    }

    return [
      '#theme' => 'instagram_social_feeds_block',
      '#data' => $json_response,
      '#config' => ['insta_image_resolution' => $insta_image_resolution, 'insta_likes' => $insta_likes],
      '#error_message' => $error_message,
    ];
  }

}
