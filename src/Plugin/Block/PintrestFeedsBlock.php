<?php

namespace Drupal\social_feeds_block\Plugin\Block;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use GuzzleHttp\ClientInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Pintrest feeds' block.
 *
 * @Block(
 *   id = "pintrest_feeds_block",
 *   admin_label = @Translation("Pintrest social feeds")
 * )
 */
class PintrestFeedsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Contains the configuration object factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configfactory;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, ClientInterface $http_client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configfactory = $config_factory;
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition, $container->get('config.factory'), $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $feed_pin_data = '';
    $error_message = '';

    $values = $this->configfactory->getEditable('config.pintrest_social_feeds');

    $pintrest_user_name = $values->get('pintrest_user_name');
    if (isset($pintrest_user_name)) {
      $url = "https://api.pinterest.com/v3/pidgets/users/" . $pintrest_user_name . "/pins?limit=2";
      $response = $this->httpClient->get($url, ['headers' => ['Accept' => 'text/json']]);
      $feeds = $response->getBody()->getContents();

      $feeds = json_decode($feeds, TRUE);
      $feed_pin_data = $feeds['data']['pins'];

      if ($feeds['status'] != 'success') {
        $error_message = 'Wrong pintrest User Name .';
      }
    }
    else {
      $error_message = "pintrest User Name Missing.";
    }

    return [
      '#theme' => 'pintrest_social_feeds_block',
      '#data' => $feed_pin_data,
      '#error_message' => $error_message,
    ];
  }

}
