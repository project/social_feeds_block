<?php

namespace Drupal\social_feeds_block\Plugin\Block;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use GuzzleHttp\ClientInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Facebook feeds' block.
 *
 * @Block(
 *   id = "fb_feeds_block",
 *   admin_label = @Translation("Facebook social feeds")
 * )
 */
class FacebookFeedsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Contains the configuration object factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configfactory;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, ClientInterface $http_client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configfactory = $config_factory;
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition, $container->get('config.factory'), $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $fb_feeds = '';
    $error_message = '';

    $values = $this->configfactory->getEditable('config.facebook_social_feeds_block');

    $fb_app_name = $values->get('fb_app_name');
    $fb_app_id = $values->get('fb_app_id');
    $fb_secret_id = $values->get('fb_secret_id');
    $fb_no_feeds = $values->get('fb_no_feeds');

    if (isset($fb_app_name) && isset($fb_app_name) && isset($fb_app_id) && isset($fb_secret_id)) {
      $fbfeeds = "https://graph.facebook.com/" . $fb_app_name . "/feed?access_token=" . $fb_app_id . "|" . $fb_secret_id . '&fields=link,message,description&limit=' . $fb_no_feeds;
      $response = $this->httpClient->get($fbfeeds, ['headers' => ['Accept' => 'text/plain']]);
      $data = $response->getBody();
      $obj = json_decode($data, TRUE);
      $fb_feeds = $obj['data'];
    }
    else {
      $error_message = 'API Cridentials are Missing.';
    }

    return [
      '#theme' => 'fb_social_feeds_block',
      '#data' => $fb_feeds,
      '#error_message' => $error_message,
    ];
  }

}
